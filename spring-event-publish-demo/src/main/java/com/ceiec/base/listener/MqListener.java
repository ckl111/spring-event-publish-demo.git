package com.ceiec.base.listener;

import com.ceiec.base.applicationevent.SystemEventType;
import com.ceiec.base.event.CommonApplicationEvent;
import com.ceiec.base.event.ICommonApplicationEventListener;
import com.ceiec.base.event.IEventType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * desc:
 *
 * @author : ckl
 * creat_date: 2019/11/16 0016
 * creat_time: 9:56
 **/
@Component
@Slf4j
public class MqListener implements ICommonApplicationEventListener{


    @Override
    public boolean supportsEventType(CommonApplicationEvent event) {
        return true;
    }

    @Override
    public void onApplicationEvent(CommonApplicationEvent event) {
        log.info("{}",event);
        IEventType eventType = event.getIEventType();
        if (eventType == SystemEventType.FINISH_INCIDENT_APPEAL) {
            // 完成接警
            /**
             * todo:
             */
        }
    }


}
