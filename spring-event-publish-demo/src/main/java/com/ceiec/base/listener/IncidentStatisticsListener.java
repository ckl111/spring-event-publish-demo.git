package com.ceiec.base.listener;

import com.ceiec.base.applicationevent.SystemEventType;
import com.ceiec.base.event.CommonApplicationEvent;
import com.ceiec.base.event.ICommonApplicationEventListener;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * desc:
 * 处警统计listener
 * @author : ckl
 * creat_date: 2019/11/16 0016
 * creat_time: 9:56
 **/
@Component
@Slf4j
public class IncidentStatisticsListener implements ICommonApplicationEventListener{

    @Override
    public boolean supportsEventType(CommonApplicationEvent commonApplicationEvent) {
        return true;
    }


    @Override
    public void onApplicationEvent(CommonApplicationEvent event) {
        log.info("{}",event);
        if (event.getIEventType() == SystemEventType.FINISH_INCIDENT_APPEAL) {

        }

    }
}
