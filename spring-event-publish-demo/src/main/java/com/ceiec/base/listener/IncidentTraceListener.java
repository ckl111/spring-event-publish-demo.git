package com.ceiec.base.listener;

import com.ceiec.base.applicationevent.SystemEventType;
import com.ceiec.base.event.CommonApplicationEvent;
import com.ceiec.base.event.ICommonApplicationEventListener;
import com.ceiec.base.eventmsg.*;
import com.ceiec.base.service.IIncidentTraceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * desc:
 * 接警统计listener
 * @author : ckl
 * creat_date: 2019/11/16 0016
 * creat_time: 9:56
 **/
@Component
@Slf4j
public class IncidentTraceListener implements ICommonApplicationEventListener{
    @Autowired
    private IIncidentTraceService iIncidentTraceService;

    @Override
    public boolean supportsEventType(CommonApplicationEvent event) {
        return true;
    }

    @Override
    public void onApplicationEvent(CommonApplicationEvent event) {
        log.info("{}",event);
        Object data = event.getData();
        if (event.getIEventType() == SystemEventType.FINISH_INCIDENT_APPEAL) {
            FinishIncidentDisposalEventMsg msg = (FinishIncidentDisposalEventMsg) data;
            iIncidentTraceService.finishIncidentDisposal(msg);
        }
    }
}
