/**
 *
 */
package com.ceiec.base.applicationevent;


import com.ceiec.base.event.IEventType;
/**
 * desc: 事件类型枚举
 * @author: ckl
 * creat_date: 2019/12/2 0002
 * creat_time: 13:46
 **/
public enum SystemEventType implements IEventType {

    FINISH_INCIDENT_APPEAL(3, "接警完成");

    /**
     * 消息码
     */
    private  int code;
    /**
     * 内容
     */
    private String content;


    SystemEventType(int code, String content) {
        this.code = code;
        this.content = content;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getContent() {
        return content;
    }




}
