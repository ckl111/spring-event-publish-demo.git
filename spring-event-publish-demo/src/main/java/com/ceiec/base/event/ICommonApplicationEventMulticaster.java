package com.ceiec.base.event;

import org.springframework.context.event.ApplicationEventMulticaster;

/**
 * desc:
 * 参考spring的设计
 * {@link ApplicationEventMulticaster}
 * @author : ckl
 * creat_date: 2019/11/16 0016
 * creat_time: 10:40
 **/
public interface ICommonApplicationEventMulticaster {
    /**
     * Add a listener to be notified of all events.
     * @param listener the listener to add
     */
    void addApplicationListener(ICommonApplicationEventListener<?> listener);



    /**
     * Remove a listener from the notification list.
     * @param listener the listener to remove
     */
    void removeApplicationListener(ICommonApplicationEventListener<?> listener);



    /**
     * Remove all listeners registered with this multicaster.
     * <p>After a remove call, the multicaster will perform no action
     * on event notification until new listeners are being registered.
     */
    void removeAllListeners();

    /**
     * Multicast the given application event to appropriate listeners.
     * @param event the event to multicast
     */
    void multicastEvent(CommonApplicationEvent event);


}
