package com.ceiec.base.event;

import org.springframework.context.ApplicationListener;

import java.util.EventListener;

/**
 * desc:
 * 参考spring
 * {@link ApplicationListener}
 * @author : ckl
 * creat_date: 2019/11/16 0016
 * creat_time: 10:45
 **/
public interface ICommonApplicationEventListener<E extends CommonApplicationEvent> extends EventListener {


    boolean supportsEventType(E event );


    /**
     * Handle an application event.
     * @param event the event to respond to
     */
    void onApplicationEvent(E event);
}
