package com.ceiec.base.service.impl;

import com.ceiec.base.eventmsg.FinishIncidentDisposalEventMsg;
import com.ceiec.base.service.IIncidentTraceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * desc:
 *
 * @author : ckl
 * creat_date: 2019/12/2 0002
 * creat_time: 14:36
 **/
@Slf4j
@Service
public class IncidentTraceServiceImpl implements IIncidentTraceService {

    @Override
    public void finishIncidentDisposal(FinishIncidentDisposalEventMsg msg) {
        /**
         * 对消息进行处理
         */
        log.info("processing msg:{}", msg);
    }
}
