package com.ceiec.base.service.impl;

import com.ceiec.base.applicationevent.SystemEventType;
import com.ceiec.base.event.CommonApplicationEvent;
import com.ceiec.base.event.CommonApplicationEventMulticaster;
import com.ceiec.base.eventmsg.FinishIncidentDisposalEventMsg;
import com.ceiec.base.service.IIncidentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * desc:
 *
 * @author : ckl
 * creat_date: 2019/12/2 0002
 * creat_time: 14:27
 **/
@Slf4j
@Service
public class IIncidentServiceImpl implements IIncidentService {

    @Autowired
    private CommonApplicationEventMulticaster commonApplicationEventMulticaster;


    @Override
    public void finishIncident() {
        FinishIncidentDisposalEventMsg msg = new FinishIncidentDisposalEventMsg();
        msg.setIncidentInformationId(1111L);
        msg.setDesc("处置完成");

        CommonApplicationEvent event = new CommonApplicationEvent(SystemEventType.FINISH_INCIDENT_APPEAL,msg);
        commonApplicationEventMulticaster.multicastEvent(event);
    }
}
