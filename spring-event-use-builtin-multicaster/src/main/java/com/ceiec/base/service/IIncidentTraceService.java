package com.ceiec.base.service;

import com.ceiec.base.eventmsg.FinishIncidentDisposalEventMsg; /**
 * desc:
 *
 * @author : ckl
 * creat_date: 2019/12/2 0002
 * creat_time: 14:36
 **/
public interface IIncidentTraceService {
    void finishIncidentDisposal(FinishIncidentDisposalEventMsg msg);
}
