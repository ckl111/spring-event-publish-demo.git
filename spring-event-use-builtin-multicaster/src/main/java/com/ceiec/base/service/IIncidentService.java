package com.ceiec.base.service;

/**
 * desc:
 *
 * @author : ckl
 * creat_date: 2019/12/2 0002
 * creat_time: 14:27
 **/
public interface IIncidentService {
    /**
     * 完成警情
     */
    void finishIncident();

}
