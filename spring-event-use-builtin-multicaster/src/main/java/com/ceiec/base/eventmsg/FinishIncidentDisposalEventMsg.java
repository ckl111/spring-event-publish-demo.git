package com.ceiec.base.eventmsg;

import lombok.Data;

/**
 * desc:
 *
 * @author : ckl
 * creat_date: 2019/11/23 0023
 * creat_time: 14:47
 **/
@Data
public class FinishIncidentDisposalEventMsg {

    /**
     * 警情id
     */
    Long incidentInformationId;

    /**
     * 描述
     */
    String desc;

}
