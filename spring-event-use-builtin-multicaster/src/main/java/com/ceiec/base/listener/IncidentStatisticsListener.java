package com.ceiec.base.listener;

import com.ceiec.base.applicationevent.SystemEventType;
import com.ceiec.base.event.CommonApplicationEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * desc:
 * 处警统计listener
 * 本listener，通过加@Component注解，被加入到spring的监听器集合
 * @author : ckl
 * creat_date: 2019/11/16 0016
 * creat_time: 9:56
 **/
@Component
@Slf4j
public class IncidentStatisticsListener implements ApplicationListener<CommonApplicationEvent> {



    @Override
    public void onApplicationEvent(CommonApplicationEvent event) {
        log.info("receive event:{}",event);
    }
}
