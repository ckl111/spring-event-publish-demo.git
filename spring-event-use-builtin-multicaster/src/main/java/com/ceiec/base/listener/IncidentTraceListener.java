package com.ceiec.base.listener;

import com.ceiec.base.applicationevent.SystemEventType;
import com.ceiec.base.event.CommonApplicationEvent;
import com.ceiec.base.eventmsg.FinishIncidentDisposalEventMsg;
import com.ceiec.base.service.IIncidentTraceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * desc:
 * 接警跟踪listener
 * 本listener，是通过在application.properties中指定context.listener.classes来纳入spring监听器集合
 * @author : ckl
 * creat_date: 2019/11/16 0016
 * creat_time: 9:56
 **/
@Slf4j
public class IncidentTraceListener implements ApplicationListener<CommonApplicationEvent>{

    @Override
    public void onApplicationEvent(CommonApplicationEvent event) {
        log.info("{}",event);
    }
}
