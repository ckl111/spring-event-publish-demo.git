package com.ceiec.base.listener;

import com.ceiec.base.event.CommonApplicationEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.context.event.EventListenerMethodProcessor;
import org.springframework.stereotype.Component;

/**
 * desc:
 *
 * @author : caokunliang
 * creat_date: 2019/12/4 0004
 * creat_time: 9:54
 **/
@Component
@Slf4j
public class MyEventHandle {
    /**
     * 参数任意(为Object）的时候所有事件都会监听到
     * 所有，该参数事件，或者其子事件（子类）都可以接收到
     * 这个注解，可以参考 {@link EventListenerMethodProcessor}
     * 这个处理器，是一个内部bean，在初始化的时候，会来扫描所有的@EventListener方法
     */
    @EventListener
    public void event(CommonApplicationEvent event){
        log.info("{}",event);
    }

}
