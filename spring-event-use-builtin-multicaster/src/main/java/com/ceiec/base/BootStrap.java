package com.ceiec.base;

import com.ceiec.base.listener.IncidentStatisticsListener;
import com.ceiec.base.service.IIncidentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * desc:
 *
 * @author : ckl
 * creat_date: 2019/12/2 0002
 * creat_time: 14:25
 **/
@SpringBootApplication
@RestController
@Slf4j
public class BootStrap {

    @Autowired
    private IIncidentService iIncidentService;

    @RequestMapping("/test.do")
    public String finishIncident() {
        iIncidentService.finishIncident();
        return "success";
    }

    public static void main(String[] args) {
        ConfigurableApplicationContext context = new SpringApplicationBuilder(BootStrap.class).web(WebApplicationType.SERVLET).run(args);
//        context.addApplicationListener(new IncidentStatisticsListener());
    }
}
