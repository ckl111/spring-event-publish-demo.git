package com.ceiec.base.event;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.context.ApplicationEvent;

import java.util.EventObject;

/**
 * desc:
 * 参考spring的设计
 * {@link org.springframework.context.ApplicationEvent}
 *
 * 该类为抽象类，可自由继承该类，定义自己的事件类，比如接处警，可以分开定义两大类：
 * 接警事件、处警事件
 * 也可以粒度更细一些，比如接警完成事件、开始处警事件、转警事件、联合处警事件、上报事件等
 *
 * 本类继承了{@link EventObject}，该父类中的source属性的意义为事件源
 * 比如，在java以前开发的客户端程序中，鼠标在xxx button上点击，会创建一个事件，这时候，source就是那个button；
 * 再比如，在web界面上点击下拉框，弹出下拉列表，此时，下拉框就是source
 * 当然，某些时候，这个source的意义不明确，比如接警完成事件，source是什么呢？可以是当前用户id，也可以是请求的整个上下文
 * 主要还是看listener那边，需要什么，这里就响应地传什么。
 *
 * 当然，也可以在自定义的子类中，定义其他字段，存储事件的信息，方便listener来处理
 *
 * @author : ckl
 * creat_date: 2019/11/16 0016
 * creat_time: 10:34
 **/
@Data
@Accessors(chain = true)
public  class  CommonApplicationEvent extends ApplicationEvent{
    /**
     * 事件类型
     */
    private  IEventType iEventType;

    /**
     * 事件携带的数据
     */
    private Object data;


    public CommonApplicationEvent(IEventType iEventType, Object data) {
        super(data);
        this.iEventType = iEventType;
        this.data = data;
    }
}
